DROP database IF EXISTS EpamTask;

CREATE database EpamTask;

USE EpamTask;

-- Table: teams
CREATE TABLE teams (
	id INTEGER PRIMARY KEY autoincrement,
	name VARCHAR(10)
);

-- Table: users
CREATE TABLE users (
	id INTEGER PRIMARY KEY autoincrement,
	login VARCHAR(10) UNIQUE
);

-- Table: users_teams
CREATE TABLE users_teams (
	user_id INTEGER REFERENCES users(id) on delete cascade,
	team_id INTEGER REFERENCES teams(id) on delete cascade,
	UNIQUE (user_id, team_id)
);

INSERT INTO users VALUES (0, 'ivanov');
INSERT INTO teams VALUES (0, 'teamA');

SELECT * FROM users;
SELECT * FROM teams;
SELECT * FROM users_teams;
